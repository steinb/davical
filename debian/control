Source: davical
Section: web
Priority: extra
Maintainer: Davical Development Team <davical-devel@lists.sourceforge.net>
Uploaders: Andrew McMillan <awm@debian.org>,
 Florian Schlichting <fsfs@debian.org>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9),
 libawl-php (>= 0.56-1~), libawl-php (<< 0.57),
 gettext,
 php5-cli,
 rst2pdf
Vcs-git: https://gitlab.com/davical-project/davical.git
Vcs-browser: https://gitlab.com/davical-project/davical
Homepage: http://www.davical.org/

Package: davical
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends},
 libawl-php (>= 0.56-1~), libawl-php (<< 0.57),
 libdbd-pg-perl,
 libyaml-perl,
 php5,
 php5-cli,
 php5-pgsql,
 postgresql-client
Recommends: php5-curl,
 postgresql
Suggests: php5-ldap
Description: PHP CalDAV and CardDAV Server
 The DAViCal CalDAV Server is designed to store CalDAV calendars and
 CardDAV addressbooks, such as those from Evolution, Sunbird/Lightning,
 Mulberry, iCal, iPhone or SOHO Organizer, in a central location,
 providing shared calendars, free/busy publication and a
 basic administration interface.

Package: davical-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: PHP CalDAV and CardDAV Server - technical documentation
 The DAViCal CalDAV Server is designed to store CalDAV calendars and
 CardDAV addressbooks, such as those from Evolution, Sunbird/Lightning,
 Mulberry, iCal, iPhone or SOHO Organizer, in a central location,
 providing shared calendars, free/busy publication and a
 basic administration interface.
 .
 This package contains detailed technical documentation for the
 classes and methods in the DAViCal programs. Some user-centric
 configuration documentation is also included.
